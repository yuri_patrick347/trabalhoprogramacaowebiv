package service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UsuarioDao;
import model.Usuario;

@WebServlet("/UsuarioService")
public class UsuarioService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String ID_USUARIO = "idUsuario";

	private static final String NOM_USUARIO = "nomeUsuario";

	private static final String EMAIL_USUARIO = "emailUsuario";

	private static final String SENHA_USUARIO = "senhaUsuario";

	UsuarioDao dao = new UsuarioDao();
	Usuario usuario = new Usuario();

	public void adiciona(HttpServletRequest request) throws ServletException, IOException {
		try {

			String nome = request.getParameter(NOM_USUARIO);
			String email = request.getParameter(EMAIL_USUARIO);
			String senha = request.getParameter(SENHA_USUARIO);

			usuario.setNome(nome);
			usuario.setEmail(email);
			usuario.setSenha(senha);

			dao.adiciona(usuario);


		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}
	
	public void atualiza(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {

			Usuario usuario = new Usuario();
			String id = request.getParameter(ID_USUARIO);
			String nome = request.getParameter(NOM_USUARIO);
			String email = request.getParameter(EMAIL_USUARIO);
			String senha = request.getParameter(SENHA_USUARIO);

			usuario.setId(Integer.parseInt(id));
			usuario.setNome(nome);
			usuario.setEmail(email);
			usuario.setSenha(senha);

			new UsuarioDao().atualizar(usuario);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void exclui(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			Usuario usuario = new Usuario();
			usuario.setId(Integer.parseInt(id));
			new UsuarioDao().excluir(usuario);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	public void getAll(HttpServletRequest request) throws ServletException, IOException {
		try {
			List<Usuario> usuarios = new UsuarioDao().getAll();
			request.setAttribute("usuarios", usuarios);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void getUsuarioById(HttpServletRequest request)
			throws ServletException, IOException {
		try {
			String id = request.getParameter("id");
			usuario = new UsuarioDao().getId(Integer.parseInt(id));
			request.setAttribute("usuario", usuario);
			// request.setAttribute("produtoId", Integer.parseInt(id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	

}
