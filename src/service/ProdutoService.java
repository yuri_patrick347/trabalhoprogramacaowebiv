package service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ProdutoDao;
import model.Produto;

@WebServlet("/ProdutoService")
public class ProdutoService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String PROD_COD = "codProduto";

	private static final String PROD_DESC = "descProduto";

	private static final String PROD_PRECO = "precoProduto";

	private static final String PROD_QNT = "qntProduto";

	private static final String PROD_OBS = "obsProduto";

	ProdutoDao dao = new ProdutoDao();
	Produto produto = new Produto();

	public void adiciona(HttpServletRequest request) throws ServletException, IOException {
		try {

			String descricao = request.getParameter(PROD_DESC);
			String preco = request.getParameter(PROD_PRECO);
			String qnt = request.getParameter(PROD_QNT);
			String obs = request.getParameter(PROD_OBS);

			produto.setDescricao(descricao);
			produto.setPreco(Double.parseDouble(preco));
			produto.setQnt(Integer.parseInt(qnt));
			produto.setObservacao(obs);

			dao.adiciona(produto);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void atualiza(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {

			Produto produto = new Produto();
			String codigo = request.getParameter(PROD_COD);
			String descricao = request.getParameter(PROD_DESC);
			String preco = request.getParameter(PROD_PRECO);
			String qnt = request.getParameter(PROD_QNT);
			String obs = request.getParameter(PROD_OBS);

			produto.setCodigo(Integer.parseInt(codigo));
			produto.setDescricao(descricao);
			produto.setPreco(Double.parseDouble(preco));
			produto.setQnt(Integer.parseInt(qnt));
			produto.setObservacao(obs);

			new ProdutoDao().atualizar(produto);

		} catch (Exception e) {
			System.out.println("Erro : " + e.getMessage());
		}
	}

	public void exclui(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String codigo = request.getParameter("codigo");
			Produto produto = new Produto();
			produto.setCodigo(Integer.parseInt(codigo));
			new ProdutoDao().excluir(produto);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	public void getAll(HttpServletRequest request) throws ServletException, IOException {
		try {
			List<Produto> produtos = new ProdutoDao().getAll();
			request.setAttribute("produtos", produtos);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void getProdutoById(HttpServletRequest request)
			throws ServletException, IOException {
		try {
			String codigo = request.getParameter("codigo");
			produto = new ProdutoDao().getCodigo(Integer.parseInt(codigo));
			request.setAttribute("produto", produto);
			// request.setAttribute("produtoId", Integer.parseInt(id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
