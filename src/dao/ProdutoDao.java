package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Produto;

public class ProdutoDao {
	private PreparedStatement ps;
	private Connection conexao;
	private ResultSet rs;

	public void adiciona(Produto produto) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "insert into produto (descricao,preco,qnt,observacao)values (?,?,?,?)";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, produto.getDescricao());
			ps.setDouble(2, produto.getPreco());
			ps.setInt(3, produto.getQnt());
			ps.setString(4, produto.getObservacao());
			ps.executeUpdate();

		} catch (Exception e) {
			System.out
					.println("Erro ao salvar o produto : " + e.getMessage() + "\n" + "Causa do erro :" + e.getCause());
		} finally {
			conexao.close();
		}

	}

	public void atualizar(Produto produto) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "UPDATE  produto SET" + " descricao=?," + " preco=?," + " qnt =?," + " observacao =?"
					+ " WHERE codigo =?";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, produto.getDescricao());
			ps.setDouble(2, produto.getPreco());
			ps.setInt(3, produto.getQnt());
			ps.setString(4, produto.getObservacao());
			ps.setInt(5, produto.getCodigo());
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro n�o foi possivel atualizar o produto : " + e.getMessage());
		} finally {
			conexao.close();
		}
	}

	public void excluir(Produto produto) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "DELETE FROM produto WHERE codigo=?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, produto.getCodigo());

			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro ao deletar o produto: " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
	}

	public List<Produto> getAll() throws Exception {
		List<Produto> lista = new ArrayList<>();
		try {
			conexao = Conexao.getConnection();
			String sql = "select * from produto";
			ps = conexao.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Produto produto = new Produto();
				produto.setCodigo(rs.getInt("codigo"));
				produto.setDescricao(rs.getString("descricao"));
				produto.setPreco(rs.getDouble("preco"));
				produto.setQnt(rs.getInt("qnt"));
				produto.setObservacao(rs.getString("observacao"));
				lista.add(produto);
			}
		} catch (Exception e) {
			System.out.println(
					"Erro ao pesquisa lista produto, com descricao do produto " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
		return lista;
	}

	public Produto getCodigo(int codigo) throws Exception {
		Produto produto = null;
		try {
			conexao = Conexao.getConnection();
			String sql = "SELECT * FROM produto WHERE codigo =?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, codigo);
			rs = ps.executeQuery();
			while (rs.next()) {
				produto = new Produto();
				produto.setCodigo(rs.getInt("codigo"));
				produto.setDescricao(rs.getString("descricao"));
				produto.setPreco(rs.getDouble("preco"));
				produto.setQnt(rs.getInt("qnt"));
				produto.setObservacao(rs.getString("observacao"));
			}

		} catch (Exception e) {
			System.out.println("Erro ao pesquisa produto " + "\n" + e.getMessage());
		} finally {
			conexao.close();
		}
		return produto;
	}

}
