package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Usuario;


public class UsuarioDao {
	private PreparedStatement ps;
	private ResultSet rs;
	private Connection conexao;


	public void adiciona(Usuario usuario) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "insert into usuario (nome,email,senha)values (?,?,?)";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, usuario.getNome());
			ps.setString(2, usuario.getEmail());
			ps.setString(3, usuario.getSenha());
			ps.executeUpdate();

		} catch (Exception e) {
			System.out
					.println("Erro ao salvar o usuario : " + e.getMessage() + "\n" + "Causa do erro :" + e.getCause());
		} finally {
			conexao.close();
		}

	}
	
	public Usuario logar(Usuario login)throws Exception{
		String sql ="select * from usuario where email =? and senha =?";
		Usuario login2 =null;
		try {
			conexao = Conexao.getConnection();
			ps = conexao.prepareStatement(sql);
			ps.setString(1, login.getEmail());
			ps.setString(2, login.getSenha());
			rs = ps.executeQuery();
			while (rs.next()){
				login2 = new Usuario();
				login2.setEmail(rs.getString("email"));
				login2.setSenha(rs.getString("senha"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return login2;
	}
	
	public void atualizar(Usuario usuario) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "UPDATE  usuario SET" + " nome=?," + " email=?," + " senha=?"
					+ " WHERE id =?";
			ps = conexao.prepareStatement(sql);
			ps.setString(1, usuario.getNome());
			ps.setString(2, usuario.getEmail());
			ps.setString(3, usuario.getSenha());
			ps.setInt(4, usuario.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro n�o foi possivel atualizar o usuario : " + e.getMessage());
		} finally {
			conexao.close();
		}
	}
	
	public void excluir(Usuario usuario) throws Exception {
		try {
			conexao = Conexao.getConnection();
			String sql = "DELETE FROM usuario WHERE id=?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, usuario.getId());
			
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro ao deletar o usuario: "+ "\n"+ e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
	}
	
	public List<Usuario> getAll() throws Exception {
		List<Usuario> lista = new ArrayList<>();
		try {
			conexao = Conexao.getConnection();
			String sql = "select * from usuario";
			ps = conexao.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId(rs.getInt("id"));
				usuario.setNome(rs.getString("nome"));
				usuario.setEmail(rs.getString("email"));
				lista.add(usuario);
			}
		} catch (Exception e) {
			System.out.println(
					"Erro ao pesquisar lista de usuario, com descricao do usuario " + "\n" + e.getMessage() + "\n");
		} finally {
			conexao.close();
		}
		return lista;
	}
	
	public Usuario getId(int id) throws Exception {
		Usuario usuario = null;
		try {
			conexao = Conexao.getConnection();
			String sql = "SELECT * FROM usuario WHERE id =?";
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				usuario = new Usuario();
				usuario.setId(rs.getInt("id"));
				usuario.setNome(rs.getString("nome"));
				usuario.setEmail(rs.getString("email"));
			}

		} catch (Exception e) {
			System.out.println("Erro ao pesquisar usuario " + "\n" + e.getMessage());
		} finally {
			conexao.close();
		}
		return usuario;
	}
	
}
