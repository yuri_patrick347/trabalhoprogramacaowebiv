package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UsuarioService;

/**
 * Servlet implementation class UsuarioController
 */
@WebServlet({ "/usuario", "/cadastroUsuario", "/update-usuario","/getId-usuario", "/exclui-usuario", "/consulta-usuarios" })
public class UsuarioController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UsuarioService service = new UsuarioService();

	/**
	 * @SEE HTTPSERVLET#DOGET(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HTTPSERVLET#DOPOST(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String uri = request.getRequestURI();
		String path = request.getContextPath();

		// NOVO USUARIO
		if (uri.equalsIgnoreCase(path + "/usuario")) {
			System.out.println("entrei");
			request.getRequestDispatcher("/cadastroUsuario.jsp").forward(request, response);
		}

		// SALVA USUARIO
		if (uri.equalsIgnoreCase(path + "/cadastroUsuario")) {
			service.adiciona(request);
			System.out.println("entrei");
			response.sendRedirect(path + "/usuario");
		}

		// LISTAR TODOS OS USUARIOS
		if (uri.equalsIgnoreCase(path + "/consulta-usuarios")) {
			service.getAll(request);
			request.getRequestDispatcher("/consultaUsuario.jsp").forward(request, response);
		}

		// BUSCA O USUARIO POR ID
		if (uri.equalsIgnoreCase(path + "/getId-usuario")) {
			service.getUsuarioById(request);
			request.getRequestDispatcher("/altera-usuario.jsp").forward(request, response);
		}

		// ATUALIZA O USUARIO
		if (uri.equalsIgnoreCase(path + "/update-usuario")) {
			service.atualiza(request, response);
			response.sendRedirect(path + "/consulta-usuarios");
		}

		// EXCLUIR USUARIO
		if (uri.equalsIgnoreCase(path + "/exclui-usuario")) {
			service.exclui(request, response);
			response.sendRedirect(path + "/consulta-usuarios");
		}
	}

}
