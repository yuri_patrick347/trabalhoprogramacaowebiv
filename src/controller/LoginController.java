package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UsuarioDao;
import model.Usuario;

@WebServlet({ "/login", "/autentica", "/logof" })
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @SEE HTTPSERVLET#DOGET(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HTTPSERVLET#DOPOST(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executa(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uri = request.getRequestURI();
		String path = request.getContextPath();

		if (uri.equalsIgnoreCase(path + "/login")) {
			login(request, response);
		}

		if (uri.equalsIgnoreCase(path + "/autentica")) {
			logar(request, response);
		}
		if (uri.equalsIgnoreCase(path + "/logof")) {
			logof(request, response);
		}

	}

	public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}

	public void logar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String email = request.getParameter("email");
			String senha = request.getParameter("senha");

			Usuario login1 = new Usuario();
			login1.setEmail(email);
			login1.setSenha(senha);

			Usuario login = new UsuarioDao().logar(login1);

			if (login != null) {
				HttpSession session = request.getSession();
				session.setAttribute("usuario_logado", login);
				response.sendRedirect(request.getContextPath() + "/home.jsp");
			} else {
				HttpSession session = request.getSession();
				session.setAttribute("login_erro", "E-mail ou Senha invalido");
				response.sendRedirect(request.getContextPath() + "/login");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}


	public void logof(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.invalidate();
		response.sendRedirect(request.getContextPath() + "/login");
	}


}
