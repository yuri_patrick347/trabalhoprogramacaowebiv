package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.ProdutoService;

/**
 * Servlet implementation class UsuarioController
 */
@WebServlet({ "/cadastroProduto", "/salva-produto", "/update-produto", "/exclui-produto", "/consulta-produtos",
		"/id-update-produto" })
public class ProdutoController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProdutoService service = new ProdutoService();

	/**
	 * @SEE HTTPSERVLET#DOGET(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HTTPSERVLET#DOPOST(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			executa(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String uri = request.getRequestURI();
		String path = request.getContextPath();

		// NOVO USUARIO
		if (uri.equalsIgnoreCase(path + "/cadastroProduto")) {
			request.getRequestDispatcher("/cadastroProduto.jsp").forward(request, response);
		}

		// SALVA USUARIO
		if (uri.equalsIgnoreCase(path + "/salva-produto")) {
			service.adiciona(request);
			response.sendRedirect(path + "/cadastroProduto");
		}

		// LISTAR TODOS OS PRODUTOS
		if (uri.equalsIgnoreCase(path + "/consulta-produtos")) {
			service.getAll(request);
			request.getRequestDispatcher("/consultaProduto.jsp").forward(request, response);
		}

		// BUSCA PRODUTO PELO ID
		if (uri.equalsIgnoreCase(path + "/id-update-produto")) {
			service.getProdutoById(request);
			request.getRequestDispatcher("/altera-produto.jsp").forward(request, response);
		}

		// ATUALIZA O PRODUTO
		if (uri.equalsIgnoreCase(path + "/update-produto")) {
			service.atualiza(request, response);
			response.sendRedirect(path + "/consulta-produtos");
		}

		// EXCLUIR OS PRODUTOS
		if (uri.equalsIgnoreCase(path + "/exclui-produto")) {
			service.exclui(request, response);
			response.sendRedirect(path + "/consulta-produtos");
		}
	}

}
