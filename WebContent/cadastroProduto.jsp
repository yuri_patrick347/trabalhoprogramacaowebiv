<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/styleCadastroProduto.css">
<title>Cadastro Produto</title>
</head>
<body>

	<jsp:include page="home.jsp" />

	<form action="salva-produto" name="dados" method="post">

		<div class="div-externa-produto">
			<p class="p-produto"></p>
			<div class="div-interna-produto">


				<table>
					<tr>
						<td>Descri��o</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto" id="descProduto"
							name="descProduto" required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Pre�o</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto" id="precoProduto"
							name="precoProduto" required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Quantidade em Estoque</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto"
							id="qntProduto" name="qntProduto" required="required"
							style="width: 315px"></td>
					</tr>
					<tr>
						<td>Observa��o</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto-area"
							id="obsProduto" name="obsProduto" required="required"
							style="width: 315px">
						</td>
					</tr>
				</table>

			</div>
			<input type="submit" class="btn-salvar-produto" value="Salvar"> 
			<input type="reset" class="btn-cancelar-produto" value="Cancelar">

		</div>

		<div class="container">
			<span class="footer-copy">&copy; Direitos autorais reservados</span>
		</div>

	</form>



</body>
</html>