<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="css/styleCadastroProduto.css">
<title>Alterar Produto</title>
</head>
<body>

	<jsp:include page="home.jsp" />

	<form action="update-produto" name="dados" method="post">

		<div class="div-externa-produto">
			<p class="p-produto"></p>
			<div class="div-interna-produto">


				<table>
					<tr>
						<td>Codigo</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto" id="codProduto"
							value="${produto.codigo}" name="codProduto"  readonly="readonly"
							required="required" style="width: 315px"></td>
					</tr>
					<tr>
					<tr>
						<td>Descri��o</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto" id="descProduto"
							value="${produto.descricao}" name="descProduto"
							required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Pre�o</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto"
							id="precoProduto" value="${produto.preco}" name="precoProduto"
							required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Quantidade em Estoque</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto"
							value="${produto.qnt}" id="qntProduto" name="qntProduto"
							required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Observa��o</td>
					</tr>
					<tr>
						<td><input type="text" class="input-produto-area"
							value="${produto.observacao}" id="obsProduto" name="obsProduto"
							required="required" style="width: 315px"></td>
					</tr>
				</table>

			</div>
			<input type="submit" class="btn-salvar-produto" value="Salvar">

		</div>

		<div class="container">
			<span class="footer-copy">&copy; Direitos autorais reservados</span>
		</div>

	</form>



</body>
</html>