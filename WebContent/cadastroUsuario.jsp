<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style type="text/css">
html, body {
	font-family: Verdana, sans-serif;
	font-size: 12px;
	line-height: 1.5;
	overflow-x: hidden;
	background-color: #E5E5E5;
	margin: 0;
	padding: 0;
}

.input-usuario {
	padding: 4px;
	margin: 3px 0;
	font-size: 15px;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 10px;
	box-sizing: border-box;
	background: #F8FCFC;
}

.btn-salvar-usuario {
    width: 110px;
    background-color: #5C8997;
    color: #F11212;
    padding: 10px 20px;
    margin: 10px 0;
    border: none;
    border-radius: 40px;
    cursor: pointer;

}

.btn-cancelar-usuario {
    width: 110px;
    background-color: #FF8F96;
    color: #F4FFFF;
    padding: 10px 20px;
    margin: 10px 100px;
    border: none;
    border-radius: 40px;
    cursor: pointer;

}

.div-externa-usuario {
	width: 430px;
	height: 235px;
	border-radius: 5px;
	background-color: #E5E5E5;
	padding: 20px;
	margin-left: 35%;
	margin-top: 100px;
}

x
.p-usuario {
	margin-top: -9px;
}

.container {
	text-align: center;
	width:100%;
	border-radius: 5px;
    background-color: #C4C4C4;
	padding: 8px;
	margin-top:16%;
}

/*------------------Home-------------------*/
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #C4C4C4;
	border-radius: 1px;
}

li {
	float: left;
}

li a, .dropbtn {
	display: inline-block;
	color: black;
	text-align: center;
	padding: 10px 20px;
	text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
	background-color: #C4C4C4;
}

li.dropdown {
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	min-width: 160px;
	box-shadow: 10px 8px 16px 10px rgba(0, 0, 0, 0.2);
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
	text-align: left;
}

.dropdown-content a:hover {
	background-color: #C4C4C4
}

.dropdown:hover .dropdown-content {
	display: block;
}

</style>

<title>Cadastro Usu�rio</title>
</head>
<body>

	<jsp:include page="home.jsp"/>

	<form action="cadastroUsuario" name="dados" method="post">

		<div class="div-externa-usuario">
			<p class="p-produto"></p>
			<div class="div-interna-usuario">


				<table>
					<tr>
						<td>Nome</td>
					</tr>
					<tr>
						<td><input type="text" class="input-usuario" id="nomeUsuario"
							name="nomeUsuario" required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>E-mail</td>
					</tr>
					<tr>
						<td><input type=email class="input-usuario" id="emailUsuario"
							name="emailUsuario" required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Senha</td>
					</tr>
					<tr>
						<td><input type="password" class="input-usuario" id="senhaUsuario"
							name="senhaUsuario" required="required" style="width: 315px"></td>
					</tr>
				</table>

			</div>
			<input type="submit" class="btn-salvar-usuario" value="Salvar"> <input
				type="reset" class="btn-cancelar-usuario" value="Cancelar">

		</div>
		
		<div class="container">
			<span class="footer-copy">&copy; Direitos autorais reservados</span>
		</div>
		
	</form>
</body>
</html>