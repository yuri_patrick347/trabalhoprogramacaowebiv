<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/styleConsultas.css">
<style type="text/css">
.rod {
	text-align: center;
	width:100%;
	border-radius: 5px;
    background-color: #C4C4C4;
	padding: 11px;
	margin-top:6%;
}
</style>
<title>Consulta de Produto</title>
</head>
<body>
	<jsp:include page="home.jsp" />
	
	<br>

	<p class="p-produto">Informe parte do nome do produto</p>
	<input type="text" id="myInput" onkeyup="myFunction()">
	
	<input type="submit" class="btn-pesquisar-produto" value="Salvar"> 

	<table style="margin-left: -45px">
		<thead>
			<tr>
				<th>Codigo</th>
				<th>Descri��o</th>
				<th>R$ Unit�rio</th>
				<th>Estoque</th>
				<th>Opera��es</th>
			</tr>
		</thead>
	</table>

	<div class="div-lista">

		<section>

			<div class="tbl-content">

				<table id="myTable">

					<tbody>
						<c:forEach var="produto" items="${produtos}">
							<tr>
								<td>${produto.codigo}</td>
								<td>${produto.descricao}</td>
								<td>${produto.preco}</td>
								<td>${produto.qnt}</td>
								
								<td style="width: 65px"><a
									href="<c:url value="/id-update-produto?codigo=${produto.codigo}"/>">Alterar</a></td>

								<td style="width: 50px"><a
									href="<c:url value="/exclui-produto?codigo=${produto.codigo}"/>"
									onclick="return confirm('Deseja realmente excluir o Produto')">Excluir</a></td>

							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</section>
	</div>
	
	<div class="rod">
			<span class="footer-copy">&copy; Direitos autorais reservados</span>
	</div>
	
	<script type="text/javascript">
		$(window).on(
				"load resize ",
				function() {
					var scrollWidth = $('.tbl-content').width()
							- $('.tbl-content table').width();
					$('.tbl-header').css({
						'padding-right' : scrollWidth
					});
				}).resize();
	</script>
	<script>
		function myFunction() {
			var input, filter, table, tr, td, i;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();
			table = document.getElementById("myTable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];
				if (td) {
					if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	</script>
</body>
</html>
