<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style type="text/css">
html, body {
	font-family: Verdana, sans-serif;
	font-size: 12px;
	line-height: 1.5;
	overflow-x: hidden;
	padding: 0;
	margin: 0;
}

body {
	background-color: #E5E5E5;
}

.btn-login {
	width: 110px;
	background-color: #5C8997;
	color: #F11212;
	padding: 10px 20px;
	border: none;
	border-radius: 3px;
	cursor: pointer;
}

.btn-cadastrar {
	width: 110px;
	background-color: #5C8997;
	color: #F11212;
	padding: 10px 20px;
	margin: 0px 10px;
	border: none;
	border-radius: 3px;
	cursor: pointer;
}

.div-login {
	width: 300px;
	height: 160px;
	border-radius: 5px;
	padding: 20px;
	margin-left: 35%;
	margin-top: 150px;
}

.div-error {
	width: 300px;
	height: 1px;
	border-radius: 5px;
	background-color: white;
	margin-left: 36%;
	padding: 20px;
}

.p-error-login {
	margin-left: 25%;
	margin-top: -10px;
	color: red;
}

.input-login {
	padding: 4px;
	margin: 5px 0;
	font-size: 15px;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 10px;
	box-sizing: border-box;
}

.p-login {
	margin-top: -10px;
}

.p-error-login {
	margin-left: 25%;
	margin-top: -10px;
	color: red;
}

/*------------------Home-------------------*/
ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #C4C4C4;
	border-radius: 1px;
}

li {
	float: left;
}

li a, .dropbtn {
	display: inline-block;
	color: black;
	text-align: center;
	padding: 10px 20px;
	text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
	background-color: #C4C4C4;
}

li.dropdown {
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	min-width: 160px;
	box-shadow: 10px 8px 16px 10px rgba(0, 0, 0, 0.2);
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
	text-align: left;
}

.dropdown-content a:hover {
	background-color: #C4C4C4
}

.dropdown:hover .dropdown-content {
	display: block;
}

.rod {
	text-align: center;
	width:100%;
	border-radius: 5px;
    background-color: #C4C4C4;
	padding: 11px;
	margin-top:17%;
}

</style>
<title></title>
</head>
<body>

	<jsp:include page="home.jsp" />

	<div class="div-login">

		<form action="autentica" accept-charset="UTF-8" method="post">

			<label class="lbl-login">E-mail</label><br /> <input type="email"
				class="input-login" name="email" style="width: 315px"><br />

			<label class="lbl-login">Senha</label><br /> <input type="password"
				class="input-login" name="senha" style="width: 315px"> <br />
			<br /> <input type="submit" class="btn-login" value="Login">

			<a href="usuario"><button type="button" class="btn-cadastrar">Cdastrar</button></a>

		</form>
	</div>


	<c:if test="${not empty login_erro}">

		<div class="div-error">
			<p class="p-error-login">${login_erro}!</p>
		</div>

	</c:if>

	<div class="rod">
		<span class="footer-copy">&copy; Direitos autorais reservados</span>
	</div>


</body>
<%
	session.removeAttribute("login_erro");
%>
</html>