<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/styleHome.css">
<title></title>
</head>
<body>

	<ul>
		<li><a class="active" href="./">Home</a></li>

		<li class="dropdown"><a href="#" class="dropbtn">Cadastros</a>
			<div class="dropdown-content">
				<a href="./usuario">Cadastro Usuario</a> 
				<a href="./cadastroProduto">Cadastro Produto</a>

			</div></li>
		<li class="dropdown"><a href="#" class="dropbtn">Consulta</a>
			<div class="dropdown-content">
				<a href="./consulta-produtos">Consulta Produto</a> <a
					href="./consulta-usuarios">Consulta Usuario</a>
				<li style="float: right"><a class="active" href="./logof">Login</a></li>
			</div></li>
	</ul>
</body>
</html>