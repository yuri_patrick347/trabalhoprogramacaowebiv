<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css"
	href="css/styleCadastroUsuario.css">
<title>Alterar Usu�rio</title>
</head>
<body>

	<jsp:include page="home.jsp" />

	<form action="update-usuario" name="dados" method="post">

		<div class="div-externa-usuario">
			<p class="p-produto"></p>
			<div class="div-interna-usuario">


				<table>
					<tr>
						<td>Id</td>
					</tr>
					<tr>
						<td><input type="text" class="input-usuario" id="idUsuario" value="${usuario.id}"
							name="idUsuario" required="required" style="width: 315px"></td>
					</tr>
					
					<tr>
						<td>Nome</td>
					</tr>
					<tr>
						<td><input type="text" class="input-usuario" id="nomeUsuario" value="${usuario.nome}"
							name="nomeUsuario" required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>E-mail</td>
					</tr>
					<tr>
						<td><input type=email class="input-usuario" id="emailUsuario" value="${usuario.email}"
							name="emailUsuario" required="required" style="width: 315px"></td>
					</tr>
					<tr>
						<td>Senha</td>
					</tr>
					<tr>
						<td><input type=password class="input-usuario" id="senhaUsuario" value="${usuario.senha}"
							name="senhaUsuario" required="required" style="width: 315px"></td>
					</tr>
					
				</table>

			</div>
			<input type="submit" class="btn-salvar-usuario" value="Salvar">

		</div>

		<div class="container">
			<span class="footer-copy">&copy; Direitos autorais reservados</span>
		</div>

	</form>
</body>
</html>